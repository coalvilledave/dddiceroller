var app = new Vue({
    el: '#app',
    computed: {
        // a computed getter
        total: function () {
            var result = 0;

            for(var i = 0;i < this.currentRolls.length;i++) {
                result+= this.currentRolls[i].value;
            }

            return result;
        },
        showTotal: function() {
            return this.currentRolls.length > 1;
        }
      },
    methods: {
        roll: function (dice) {
           this.showRoll(dice);
        },
        getRandomValue(max) {
            return Math.floor(Math.random() * Math.floor(max - 1)) + 1;
        },
        showRoll: function(max) {
            
            var self = this;

            if(self.startNewRoll === true)
            {
                self.currentRolls = [];
                self.startNewRoll = false;
            }

            var inc = 0;
            var currentIndex = self.currentRolls.length;
            self.currentRolls.push({ dice: max, value: 1, rolling: true });
            var current = self.currentRolls[currentIndex];

            for(var i = 0;i < 15;i++) {
                var waitTime = (i * 10) * i;

                setTimeout(function() {
                    current.value = ((inc++) % max) + 1;
                }, waitTime);
            }


            setTimeout(function() {
                current.value = self.getRandomValue(max);
                current.rolling = false;
            }, waitTime);

            setTimeout(function() {
                self.startNewRoll = true;
            }, 3000);
        },
        isRolling: function(dice) {
            var result = false;

            for(var i = 0;i < this.currentRolls.length;i++)
            {
                var current = this.currentRolls[i];

                if(current.dice === dice && current.rolling === true)
                {
                    result = true;
                    break;
                }
            }

            return result;
        },
        stringIsEmpty: function(value) {
            return value ? value.trim().length == 0 : true;
        },
        getDie: function() {
            var results = [];
            var validDie = ["4", "6", "8", "10", "12", "20"];
            var profiles = [];
            profiles["dd"] = [4, 6, 8, 10, 12, 20];

            var params = this.getQueryParams();
            var die = [];

            if(params["die"])
                die = params["die"].split("-");
            
            if(die.length === 0)
                results = [6];
            else {
                for(var i = 0;i < die.length;i++)
                {
                    if(this.stringIsEmpty(die[i]) === false) {
                        // If die is a profile
                        if(profiles[die[i]] !== undefined)
                        {
                            for(var t = 0;t < profiles[die[i]].length;t++)
                                results.push(profiles[die[i]][t]);
                        }
                        else if(validDie.indexOf(die[i]) !== -1)
                        {
                            var val = parseInt(die[i]);
                            results.push(val);
                        }
                    }
                }
            }

            return results;
        },
        getQueryParams: function() {
            var qs = window.location.search.substring(1);

            qs = qs.split('+').join(' ');
        
            var results = {},
                tokens,
                re = /[?&]?([^=]+)=([^&]*)/g;
        
            while (tokens = re.exec(qs)) {
                results[decodeURIComponent(tokens[1])] = decodeURIComponent(tokens[2]);
            }
        
            return results;
        },
        toggleHelp: function() {
            this.isHelpDisplayed = !this.isHelpDisplayed;
        }
    },
    created: function () {
        // `this` points to the vm instance
        this.die = this.getDie();
        this.currentRolls = [ { dice: this.die[0], value: "?", rolling: false } ]
        },
    data: { 
      currentRolls: [],
      startNewRoll: true,
      die: [],
      isHelpDisplayed: false
    }
  })